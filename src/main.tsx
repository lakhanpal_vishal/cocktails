import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import ReactRouterSetup from "./ReactRouterSetup";

ReactDOM.render(
  <React.StrictMode>
    <div className="min-h-screen bg-gray-100">
      <ReactRouterSetup></ReactRouterSetup>
    </div>
  </React.StrictMode>,
  document.getElementById("root")
);
