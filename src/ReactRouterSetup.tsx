import React from "react";
// react router
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
// pages
import Home from "./Home";
import About from "./About";

//import Person from "./Person";

import Navbar from "./components/Navbar";
import CocktailDetails from "./components/CocktailDetails";
// navbar

const ReactRouterSetup = () => {
  return (
    <Router>
      <Navbar />
      <Switch>
        <Route exact path="/">
          <Home />
        </Route>
        <Route path="/about">
          <About />
        </Route>

        <Route
          path="/cocktaildetails/:id"
          children={<CocktailDetails />}
        ></Route>
      </Switch>
    </Router>
  );
};

export default ReactRouterSetup;
