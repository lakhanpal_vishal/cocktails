import React from "react";

const About = () => {
  return (
    <div>
      <div className="flex mx-auto  justify-center items-center mt-20 flex-col capitalize w-2/5">
        <h1 className="text-3xl font-bold tracking-widest">About us</h1>

        <p className="mt-14 tracking-widest text-l  leading-8">
          Lorem ipsum dolor sit amet consectetur adipisicing elit. Molestiae
          repudiandae architecto qui adipisci in officiis, aperiam sequi atque
          perferendis eos, autem maiores nisi saepe quisquam hic odio
          consectetur nobis veritatis quasi explicabo obcaecati doloremque?
          Placeat ratione hic aspernatur error blanditiis?
        </p>
      </div>
    </div>
  );
};

export default About;
