import React, { FC } from "react";
import { Link } from "react-router-dom";
import ICocktail from "../models/ICocktail";

// type CT = {
//   id: number;
//   capital: string;
//   picture: string;
//   glass: string;
//   type: string;
// };

const Cocktail: FC<ICocktail> = ({ id, capital, picture, glass, type }) => {
  return (
    <div className="max-w-sm overflow-hidden  shadow-2xl rounded-lg bg-white ">
      <img
        className="h-80 w-full"
        src={picture}
        alt="Sunset in the mountains"
      />
      <div className="px-6 py-4">
        <div className="font-bold text-4xl mb-2 tracking-widest opacity-90 ">
          {capital}
        </div>
        <p className="font-bold font-mono text-lg  opacity-80 tracking-widest">
          {glass}
        </p>
        <p className="font-base font-mono text-base  opacity-40 ">{type}</p>
        <Link to={`/cocktaildetails/${id}`}>
          <button
            className="mt-1 h-8 w-18 tracking-widest text-left text-sm bg-green-800 uppercase hover:bg-green-200 text-white
              hover:text-green-900 py-1 px-6 border border-none hover:border-transparent rounded
              focus:outline-none"
          >
            Details
          </button>
        </Link>
      </div>
    </div>
  );
};
export default Cocktail;
