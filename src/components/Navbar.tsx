import React from "react";
import { Link } from "react-router-dom";
export default function Navbar() {
  return (
    <nav className="relative flex flex-wrap items-center justify-between px-2 py-3 bg-white mb-3 border-gray-500 border-b-4 h-20">
      <div className="container px-6 mx-auto flex flex-wrap items-center justify-between">
        <div className="w-full relative flex justify-between lg:w-auto  px-4 lg:static lg:block lg:justify-start">
          <Link to="/">
            <div className="text-2xl font-semibold leading-relaxed inline-block mr-4 py-2 whitespace-nowrap  tracking-widest">
              The<span className="text-green-800">Cocktail</span>DB
            </div>
          </Link>
        </div>
        <div
          className="lg:flex flex-grow items-center"
          id="example-navbar-warning"
        >
          <ul className="flex flex-col lg:flex-row list-none ml-auto">
            <li className="nav-item">
              <div className="px-3 py-2 flex items-center text-xl  leading-snug  opacity-80 hover:opacity-75">
                <Link to="/">Home</Link>
              </div>
            </li>
            <li className="nav-item">
              <div className="px-3 py-2 ml-2 mr-4 flex items-center text-xl leading-snug  opacity-80  hover:opacity-75">
                <Link to="/about">About</Link>
              </div>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  );
}
