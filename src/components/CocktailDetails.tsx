import React, { useState, useEffect } from "react";
import { CocktailsData } from "../data/CocktailsData";
import { Link, useParams } from "react-router-dom";

const CocktailDetails = () => {
  const [name, setName] = useState<string | undefined>("");
  const { id } = useParams<{ id: string }>();

  useEffect(() => {
    const ct = CocktailsData.find((cocktail) => cocktail.id === parseInt(id));
    setName(ct?.capital);
  }, [id]);
  return (
    <div>
      <div className="flex mt-20 justify-center items-center flex-col">
        <Link to="/">
          <button
            className=" mt-4 h-8 w-18 tracking-widest text-left text-sm bg-green-800 uppercase hover:bg-green-200 text-white
              hover:text-green-900 py-1 px-6 border border-none hover:border-transparent rounded
              focus:outline-none justify-center  mx-auto           
              "
          >
            Back To Home
          </button>
        </Link>

        <p className="mt-6 text-3xl font-bold tracking-widest">{name}</p>
      </div>
    </div>
  );
};

export default CocktailDetails;
