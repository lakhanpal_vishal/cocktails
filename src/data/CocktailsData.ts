export const CocktailsData = [
  {
    id: 0,
    capital: `A1`,
    picture: "./src/img/chicago.jpg",
    glass: `Cocktail glass`,
    type: `Alcoholic`,
  },
  {
    id: 1,
    capital: `ABC`,
    picture: "./src/img/colorado.jpg",
    glass: `Shot glass`,
    type: `Alcoholic`,
  },
  {
    id: 2,
    capital: `Ace`,
    picture: "./src/img/malibu.jpg",
    glass: `Martini glass`,
    type: `Alcoholic`,
  },
  {
    id: 3,
    capital: `Adam`,
    picture: "./src/img/miami.jpg",
    glass: `Cocktail glass`,
    type: `Alcoholic`,
  },
  {
    id: 4,
    capital: `AT&T`,
    picture: "./src/img/seattle.jpg",
    glass: `Highball Glass`,
    type: `Non alcoholic`,
  },
  {
    id: 5,
    capital: `ACID`,
    picture: "./src/img/toronto.jpg",
    glass: `Shot glass`,
    type: `Non alcoholic`,
  },
];
