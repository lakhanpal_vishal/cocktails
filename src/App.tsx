import React from "react";

import { CocktailsData } from "./data/CocktailsData";
import "./App.css";
import Cocktail from "./components/Cocktail";

function App() {
  return (
    <div>
      <div className="flex flex-col items-center justify-center">
        <div className="flex flex-col items-start pl-10 pt-8 mt-20 w-2/5 border-b-4 border-r-2 border-gray-300 bg-white  rounded-lg  shadow-xl">
          <h1 className="text-lg text-green-600  font-serif font-semibold  tracking-widest capitalize ">
            search your favorite cocktail
          </h1>
          <input
            type="text"
            className="h-10 w-11/12 mb-8 mt-4  rounded-lg z-0 border-2 border-gray-300 bg-gray-100 focus:shadow focus:outline-none"
            placeholder="Search..."
          />
        </div>
      </div>
      <div className="mt-10 text-center text-4xl font-bold font-sans tracking-widest opacity-80">
        Cocktailsss
      </div>
      <br />
      <div className="flex mt-10 justify-center items-center">
        <div className="grid grid-cols-3 justify-center  gap-10 mt-4 mb-6 w-4/6">
          {CocktailsData.map((data, index) => (
            <Cocktail key={index} {...data}></Cocktail>
          ))}
        </div>
      </div>

      <div>&nbsp;</div>
    </div>
  );
}

export default App;
