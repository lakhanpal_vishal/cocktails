export default interface ICocktail {
  id: number;
  capital: string;
  picture: string;
  glass: string;
  type: string;
}
